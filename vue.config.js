/*
 * @Autor: zxlstart
 * @Date: 2023-10-24 10:02:25
 * @LastEditors: zxl1227
 * @Description:
 */
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true
})
