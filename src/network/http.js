/*
 * @Autor: zxlstart
 * @Date: 2023-10-24 19:43:50
 * @LastEditors: zxl1227
 * @Description:对axios进行二次封装
 */
import axios from 'axios'
// 有些post接口无法解析对象类型的请求参数, 可以使用qs模块(node内置模块)把对象类型的数据序列化为查询字符串结构 {name:lisi, age:10} ===> ?name=lisi&age=10
import qs from 'qs'

import { Loading } from 'element-ui'
let loading = null

// 功能1 根据环境设置axios请求地址
if (process.env.NODE_ENV == 'development') {
  axios.defaults.baseURL = 'https://mock.apifox.cn/xxx' // 开发环境的代理路径
} else if (process.env.NODE_ENV == 'production') {
  axios.defaults.baseURL = 'https://mock.apifox.cn/xxx' // 生产环境的原始路径
}

//功能2 添加超时功能
axios.defaults.timeout = 30000

// 功能3 是否允许跨域请求携带Cookie, 前端设置了该属性为true时，后端需要设置Access-Control-Allow-Origin为前端项目的源地址，不可设置为*；
axios.defaults.withCredentials = false

// // 功能4 在请求头设置(post)请求内容的编码格式，这里一般要改掉
// axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.headers['Content-Type'] = 'application/json'

// 功能5 把请求的参数由对象结构转化为查询字符串结构,  转换通过qs第三方进行转换  data就是请求参数
// axios.defaults.transformRequest = (data) => qs.stringify(data)

// 功能6 axios请求拦截 在发请求之前 对请求进行处理 一般把本地token添加到请求头里面执行登录认证, 这样在发请求时就不用考虑并处理token了
axios.interceptors.request.use(
  (request) => {
    // 请求时开始加载, target参数指定加载框显示的区域
    if (!loading) loading = Loading.service({ target: '#app' })
    // console.log("axios请求拦截",request); // config参数是请求的配置信息
    let token = sessionStorage.getItem('token') //从本地取出token
    // 登录授权 请求验证是否有token  需要授权的 API ，必须在请求头中使用 `Authorization` 字段提供 `token` 令牌(具体字段由服务器后台定义)
    token && (request.headers.Authorization = token)
    return request
  },
  (error) => {
    return Promise.reject(error) // 返回错误
  }
)

// 功能7 axios的响应拦截, 可在这里对响应数据或响应头做处理
axios.interceptors.response.use(
  (response) => {
    if (loading) {
      // 响应时结束加载
      loading.close()
      loading = null
    }
    // axios响应数据会封装到一层data字段中, 把data字段剥开,返回原始响应
    return response.data
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 功能8 显示请求时的加载框, 见请求拦截和响应拦截

// 最后,返回配置之后的axios
export default axios
