/*
 * @Autor: zxlstart
 * @Date: 2023-10-24 20:40:38
 * @LastEditors: zxl1227 
 * @Description:对接口进行分开管理
 *   this.$api.account.postRegister().then((res)=>{
          console.log(res)
        })
   模块名.方法名即可
 * 1.全参数请求
 * return http({
		      url: '/detail',
		      method: 'get',
		      params:{
		        goodsId
		      },
		      hideloading: true
	})
    2.其他参数参考下面
    3.如果有token限制，默认加token情况下，项不加token即isNeedToken:false要作为querry参数
 */
import http from '@/network/http'

const account = {
  // 注册
  postRegister(params) {
    return http.post('/register', params)
  },  
    // 注册
  postRegister(body,querry) {
    return http.post('/register', body,{querry,isNeedToken:false})
  }, 
  // 注册
  getxx(querry) {
    return http.post('/register', {querry})
  }
}

export { account }
