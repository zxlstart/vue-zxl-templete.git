/*
 * @Autor: zxlstart
 * @Date: 2023-10-24 20:39:30
 * @LastEditors: zxl1227
 * @Description:api管理中心
 */
// 1.账号接口
import { account } from '../api/account'
// 其他接口

// 2.导出接口
export default { account }
