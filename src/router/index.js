/*
 * @Autor: zxlstart
 * @Date: 2023-10-24 14:13:49
 * @LastEditors: zxl1227
 * @Description:
 */
// 1.导包
import Vue from 'vue'
import VueRouter from 'vue-router'
// 2.安装插件
Vue.use(VueRouter)
// 3.创建实例对象
const router = new VueRouter({
  routes: [
    // 开头固定
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('../view/homePage/index.vue'),
      meta: { title: '首页' }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../view/login/index.vue'),
      meta: { title: '用户登录' }
    },
    // 结尾固定
    {
      path: '*',
      component: () => import('../view/notFound.vue'),
      meta: { title: '错误页' }
    }
  ]
})
// 4.路由守卫
router.beforeEach((to, from, next) => {
  // 根据回话内存是否有token来进行路由守卫，只找出无需路由守卫的
  next()
})
// 全局后置守卫
router.afterEach(function (to, from) {
  document.title = to.meta.title || '税海学堂'
})
// 5.暴露出去
export default router
