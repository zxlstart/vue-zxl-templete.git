/*
 * @Autor: zxlstart
 * @Date: 2023-10-24 10:02:25
 * @LastEditors: zxl1227
 * @Description:
 */
import Vue from 'vue'

// 引入elementui
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element, { size: 'small', zIndex: 3000 })

import App from './App.vue'

// 导入axios
import api from './network/api/index' // 导入api接口
Vue.prototype.$api = api // 将api挂载到vue的原型上复制代码

// 1.导入路由
import router from './router'
Vue.config.productionTip = false

new Vue({
  render: (h) => h(App),
  // 2.挂载路由
  router: router
}).$mount('#app')
